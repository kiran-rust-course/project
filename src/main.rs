/// A command-line tool for interpreting brainfuck code.
/// Uses the [bft_types] and [bft_interp] libraries.
use std::io::{stdin, stdout};

use anyhow::Result;
use bft_interp::VirtualMachine;
use bft_types::Program;
use clap::Parser;
use cli::Options;
use log::{error, warn};

mod cli;
mod proxy_writer;

use pretty_env_logger::formatted_builder;
use proxy_writer::ProxyWriter;

/// Uses the info stored in `options` to run the brainfuck interpreter.
fn run_bft(options: &Options) -> Result<()> {
    let program = Program::from_file(&options.program)?;
    let mut virtual_machine =
        <VirtualMachine>::new(&program, options.cells.try_into()?, options.extensible);
    virtual_machine.interpret(&mut stdin(), &mut ProxyWriter::new(&mut stdout()))?;
    Ok(())
}

/// Main program for bft
fn main() {
    let options = cli::Options::parse();
    let level = match options.verbosity {
        0 => log::LevelFilter::Warn,
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    };
    formatted_builder().filter_level(level).init();
    if let Err(err) = run_bft(&options) {
        error!("{err}");
        std::process::exit(1);
    };
}
