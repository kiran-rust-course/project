use clap::{crate_authors, Parser};
use std::num::NonZeroUsize;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[clap(author=crate_authors!(), version, about, name = "bft")]
/// A Brainfuck interpreter. This was written as part of Daniel Silverstone's
/// rust workshop.
pub(crate) struct Options {
    /// The program to interpret
    #[clap(name = "PROGRAM", parse(from_os_str))]
    pub(crate) program: PathBuf,

    /// Number of cells in the virtual-machine tape
    #[clap(name = "cells", short, long, default_value = "30000")]
    pub(crate) cells: NonZeroUsize,

    /// Allow the virtual-machine tape to be extensible
    #[clap(name = "extensible", short, long)]
    pub(crate) extensible: bool,

    /// Verbosity
    #[clap(short, long, parse(from_occurrences))]
    pub(crate) verbosity: u8,
}
