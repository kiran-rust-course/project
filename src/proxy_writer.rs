use crate::warn;
use std::io::{Cursor, Write};

/// Used as a man-in-the-middle output source.
/// We use this to check if the final output corresponds to a new line.
/// If not then add one!
pub struct ProxyWriter<'a, T> {
    output: &'a mut T,
    internal: Cursor<Vec<u8>>,
}

impl<'a, T: Write> ProxyWriter<'a, T> {
    /// Used to build new proxy writers.
    /// ```
    /// use std::io::stdout
    /// use bft::ProxyWriter
    ///
    /// let proxy_writer = ProxyWriter::new(&stdout());
    /// ```
    pub fn new(writer: &'a mut T) -> Self {
        Self {
            output: writer,
            internal: Cursor::new(Vec::<u8>::new()),
        }
    }
}

impl<'a, T: Write> Write for ProxyWriter<'a, T> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let bytes_written = self.internal.write(buf)?;
        if bytes_written < buf.len() {
            warn!(
                "ProxyWriter failed to write the last {} bytes of {:?} to its internal buffer.",
                buf.len() - bytes_written,
                buf,
            )
        }
        self.output.write(buf)
    }
    fn flush(&mut self) -> std::io::Result<()> {
        self.internal.flush()?;
        self.output.flush()
    }
}

impl<'a, T> Drop for ProxyWriter<'a, T> {
    fn drop(&mut self) {
        if let Some(byte) = self.internal.get_ref().last() {
            if byte != &b'\n' {
                println!()
            }
        }
    }
}
