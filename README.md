Questions:

* What does mut mean in passing?
* Why didn't templating error thing work?
* Why try_from(0) can't go in match
* What is happening with filter_map thing
* How get check_head in mod?
* Why doesn't `output` need to be mutable in `interpret`?
* How does parse work with clap derive?
