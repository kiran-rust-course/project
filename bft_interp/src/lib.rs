/*!
This library provides the [VirtualMachine] struct that can be used for interpreting
brainfuck code.
 */

use bft_types::Program;
use std::cmp::PartialEq;
use std::fmt::Debug;
use std::io::{Read, Write};

mod errors;

use bft_types::InstructionData;
use errors::{VMError, VMResult};

#[derive(Debug)]
pub struct VirtualMachine<'a, T = u8> {
    /// The bf program to be interpreted
    program: &'a Program,
    /// Virtual machine's tape
    tape: Vec<T>,
    /// Current position position being pointed to in the tape
    tape_head: usize,
    /// Current position being interpreted in the bf program
    program_head: usize,
    /// Whether or not the virtual machine tape can grow
    can_grow: bool,
}

pub trait VMCell: Default + Clone + TryFrom<u8> + TryInto<u8> + ToString + PartialEq {
    /// Perform a wrapped increment of the cell
    fn increment(&self) -> Self;
    /// Perform a wrapped decrement of the cell
    fn decrement(&self) -> Self;
}

impl VMCell for u8 {
    fn increment(&self) -> Self {
        self.wrapping_add(1)
    }
    fn decrement(&self) -> Self {
        self.wrapping_sub(1)
    }
}

impl<'a, T> VirtualMachine<'a, T>
where
    T: VMCell,
{
    /// Used to build a new virtual machine.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// ```
    pub fn new(program: &'a Program, len: usize, can_grow: bool) -> Self {
        Self {
            program,
            tape: vec![Default::default(); if len == 0 { 3000 } else { len }],
            tape_head: 0,
            program_head: 0,
            can_grow,
        }
    }

    /// Increment the value at the tape head.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.increment_cell().unwrap(), 1);
    /// assert_eq!(my_vm.tape_head(), 1u8);
    /// ```
    fn increment_cell(&mut self) -> VMResult {
        self.tape[self.tape_head] = self.tape_head().increment();
        Ok(self.program_head + 1)
    }

    /// Decrement the value at the tape head.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.decrement_cell().unwrap(), 1);
    /// assert_eq!(my_vm.tape_head(), u8::MAX);
    /// ```    
    fn decrement_cell(&mut self) -> VMResult {
        self.tape[self.tape_head] = self.tape_head().decrement();
        Ok(self.program_head + 1)
    }

    /// Get the last instruction being interpreted
    /// ```
    /// use bft_types::{Program, InstructionData, RawInstruction};
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let first_instr = InstructionData::new(RawInstruction::DecrementPointer, (1,5));
    /// let my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.program_head(), first_instr);
    /// ```
    fn program_head(&self) -> InstructionData {
        self.program.instructions()[self.program_head]
    }

    /// If cell at tape head is nonzero then find the instruction after the
    /// corresponding open bracket. Else, point to the next instruction after
    /// the current.
    /// Correspondence is determined by `bracket_pairs`.
    fn loop_end(&self) -> VMResult {
        let cell_zero = match T::try_from(0u8) {
            Ok(number) => number,
            _ => panic!("Cell type has no zero value"),
        };
        // let next_instruction = match self.tape_head() {
        //     cell_zero => {
        //         *self
        //             .program
        //             .bracket_pairs()
        //             .get_by_right(&self.program_head)
        //             .ok_or(VMError::Brackets {
        //                 line: self.program_head().line(),
        //                 column: self.program_head().column(),
        //                 file: self.program.file_name().to_string_lossy().to_string(),
        //             })?
        //             + 1
        //     }
        //     _ => self.program_head + 1,
        // };
        let next_instruction = if self.tape_head() != cell_zero {
            self.program
                .bracket_pairs()
                .get_by_right(&self.program_head)
                .ok_or(VMError::Brackets {
                    line: self.program_head().line(),
                    column: self.program_head().column(),
                    file: self.program.file_name().to_string_lossy().to_string(),
                })?
                + 1
        } else {
            self.program_head + 1
        };
        Ok(next_instruction)
    }

    /// Point to the corresponding closed bracket, no matter the cell value at
    /// the tape head.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("[-]", "some_file.bf").unwrap();
    /// let my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.loop_start().unwrap(), 2);
    /// ```
    fn loop_start(&self) -> VMResult {
        let next_instruction = self
            .program
            .bracket_pairs()
            .get_by_left(&self.program_head)
            .ok_or(VMError::Brackets {
                line: self.program_head().line(),
                column: self.program_head().column(),
                file: self.program.file_name().to_string_lossy().to_string(),
            })?;
        Ok(*next_instruction)
    }

    /// Read from `reader` into cell at `self.tape[self.tape_head]`.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// let mut readable_obj = std::io::Cursor::new(vec![2u8, 3u8]);
    ///
    /// assert_eq!(my_vm.read_to_cell(&mut readable_obj).unwrap(), 1);
    /// assert_eq!(my_vm.tape_head(), 2u8);
    /// ```
    fn read_to_cell(&mut self, reader: &mut impl Read) -> VMResult {
        let mut buf = vec![0u8];
        reader.read_exact(buf.as_mut_slice())?;
        match T::try_from(buf[0]) {
            Ok(value) => {
                self.tape[self.tape_head] = value;
                Ok(self.program_head + 1)
            }
            Err(_) => Err(VMError::From {
                value: buf[0].to_string(),
            }),
        }
    }

    /// Write from cell at `self.tape[self.tape_head]` into writer.
    /// If OK, returns the next instruction pointer to use.
    /// It's difficult to write a doc test for this without making the whole
    /// io module public.
    fn write_from_cell(&self, writer: &mut impl Write) -> VMResult {
        match T::try_into(self.tape_head()) {
            Ok(head_u8) => {
                writer.write_all(vec![head_u8].as_slice())?;
                Ok(self.program_head + 1)
            }
            Err(_) => Err(VMError::Into {
                value: self.tape_head().to_string(),
            }),
        }
    }

    /// Clone the cell at the tape head
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// my_vm.increment_cell();
    /// my_vm.increment_cell();
    ///
    /// assert_eq!(my_vm.tape_head(), 2u8);
    /// ```
    fn tape_head(&self) -> T {
        self.tape[self.tape_head].clone()
    }

    /// Check that the tape head is in bounds
    fn check_head(&mut self) -> VMResult {
        if !(0..self.tape.len()).contains(&self.tape_head) {
            if !self.can_grow {
                let line = self.program_head().line();
                let column = self.program_head().line();
                let file = self.program.file_name().display().to_string();
                let position = self.tape_head;
                let end = self.tape.len();
                return Err(VMError::InvalidHead {
                    file,
                    line,
                    column,
                    raw_instruction: *self.program_head().raw_instruction(),
                    position,
                    end,
                });
            } else {
                self.tape.push(Default::default());
            };
        };
        Ok(self.program_head)
    }

    /// Move tape head backwards, i.e. decrement `self.tape_head`.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// my_vm.tape_head_forwards();
    ///
    /// assert_eq!(my_vm.tape_head_backwards().unwrap(), 1);
    /// assert!(my_vm.tape_head_backwards().is_err());
    /// ```
    fn tape_head_backwards(&mut self) -> VMResult {
        self.tape_head = self.tape_head.checked_sub(1).ok_or(VMError::InvalidHead {
            line: self.program_head().line(),
            column: self.program_head().column(),
            raw_instruction: *self.program_head().raw_instruction(),
            file: self.program.file_name().display().to_string(),
            position: self.tape_head,
            end: self.tape.len(),
        })?;
        Ok(self.program_head + 1)
    }

    /// Move tape head forwards i.e. increment `self.tape_head`.
    /// If OK, returns the next instruction pointer to use.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// let my_program = Program::new("some<.+>bf[-+]", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    ///
    /// assert_eq!(my_vm.tape_head_forwards().unwrap(), 1);
    /// ```
    fn tape_head_forwards(&mut self) -> VMResult {
        self.tape_head += 1;
        self.check_head()?;
        Ok(self.program_head + 1)
    }

    /// Interpret the program.
    /// Output is written to `output` and input is taken from `input`.
    /// ```
    /// use bft_types::Program;
    /// use bft_interp::VirtualMachine;
    /// use std::io::Cursor;
    /// let my_program = Program::new("++>+++++[<+>-]>.<.<.", "some_file.bf").unwrap();
    /// let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
    /// let mut input = Cursor::new(Vec::<u8>::new());
    /// let mut output = Cursor::new(Vec::<u8>::new());
    /// my_vm.interpret(&mut input, &mut output);
    ///
    /// assert_eq!(output.get_ref(), &[0,0,7]);
    /// ```
    pub fn interpret(
        &mut self,
        mut input: &mut impl Read,
        mut output: &mut impl Write,
    ) -> Result<(), VMError> {
        while self.program_head < self.program.instructions().len() {
            self.program_head = match self.program_head().raw_instruction() {
                bft_types::RawInstruction::IncrementPointer => self.tape_head_forwards()?,
                bft_types::RawInstruction::DecrementPointer => self.tape_head_backwards()?,
                bft_types::RawInstruction::IncrementByte => self.increment_cell()?,
                bft_types::RawInstruction::DecrementByte => self.decrement_cell()?,
                bft_types::RawInstruction::PrintByte => self.write_from_cell(&mut output)?,
                bft_types::RawInstruction::StoreByte => self.read_to_cell(&mut input)?,
                bft_types::RawInstruction::LoopStart => self.loop_start()?,
                bft_types::RawInstruction::LoopEnd => self.loop_end()?,
            };
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::{Program, VirtualMachine};
    use bft_types::BFError;

    fn get_good_program() -> Result<Program, BFError> {
        let test_contents = "
            > <+CHOO-.
            ,[CHOO]
        ";
        let test_name = "test.bf";
        Program::new(test_contents, test_name)
    }

    #[test]
    fn test_constructor() {
        let good_program = get_good_program().unwrap();
        let virtual_machine = <VirtualMachine>::new(&good_program, 0, false);
        assert_eq!(virtual_machine.tape.len(), 3000);
        assert_eq!(virtual_machine.tape_head, 0);
    }

    #[test]
    fn loop_end_parsing() {
        let my_program = Program::new("[.].", "some_file.bf").unwrap();
        let mut my_vm = <VirtualMachine>::new(&my_program, 0, false);
        my_vm.program_head = 2;

        assert_eq!(my_vm.loop_end().unwrap(), 3);

        my_vm.tape[0] = 1;
        assert_eq!(my_vm.loop_end().unwrap(), 1);
    }

    #[test]
    fn test_check_head() {
        let good_program = get_good_program().unwrap();

        let mut fixed_tape = <VirtualMachine>::new(&good_program, 1, false);
        assert!(fixed_tape.check_head().is_ok());
        assert!(fixed_tape.tape_head_forwards().is_err());
        assert!(fixed_tape.check_head().is_err());

        let mut fixed_tape = <VirtualMachine>::new(&good_program, 1, true);
        assert!(fixed_tape.check_head().is_ok());
        assert!(fixed_tape.tape_head_forwards().is_ok());
    }
}
