/// This library provides types that are useful for interpreting brainfuck code.
use std::fmt;
use thiserror::Error;
use variant_count::VariantCount;

mod program;

pub use program::Program;

/// Store a brainfuck instruction
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct InstructionData {
    /// The instruction itself
    raw_instruction: RawInstruction,
    /// The line where the instruction was found
    line: usize,
    /// The column where the instruction was found
    column: usize,
}

impl InstructionData {
    /// Create an [`InstructionData`] object
    fn new(raw_instruction: RawInstruction, line_col: (usize, usize)) -> Self {
        Self {
            raw_instruction,
            line: line_col.0,
            column: line_col.1,
        }
    }
    /// Get the raw instruction
    pub fn raw_instruction(&self) -> &RawInstruction {
        &self.raw_instruction
    }
    /// Get the line number where the instruction was found
    pub fn line(&self) -> usize {
        self.line
    }
    /// Get the column number where the instruction was found
    pub fn column(&self) -> usize {
        self.column
    }
}

/// Raw brainfuck instructions
#[derive(Debug, VariantCount, PartialEq, Eq, Clone, Copy)]
pub enum RawInstruction {
    /// Corresponds to the `>` character
    IncrementPointer,
    /// Corresponds to the `<` character
    DecrementPointer,
    /// Corresponds to the `+` character
    IncrementByte,
    /// Corresponds to the `-` character
    DecrementByte,
    /// Corresponds to the `.` character
    PrintByte,
    /// Corresponds to the `,` character
    StoreByte,
    /// Corresponds to the `[` character
    LoopStart,
    /// Corresponds to the `]` character
    LoopEnd,
}

impl RawInstruction {
    /// Convert a character into the corresponding [`RawInstruction`]
    fn from_char(c: &char) -> Option<RawInstruction> {
        match c {
            '>' => Some(RawInstruction::IncrementPointer),
            '<' => Some(RawInstruction::DecrementPointer),
            '+' => Some(RawInstruction::IncrementByte),
            '-' => Some(RawInstruction::DecrementByte),
            '.' => Some(RawInstruction::PrintByte),
            ',' => Some(RawInstruction::StoreByte),
            '[' => Some(RawInstruction::LoopStart),
            ']' => Some(RawInstruction::LoopEnd),
            _ => None,
        }
    }
}

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RawInstruction::IncrementPointer => write!(f, ">"),
            RawInstruction::DecrementPointer => write!(f, "<"),
            RawInstruction::IncrementByte => write!(f, "+"),
            RawInstruction::DecrementByte => write!(f, "-"),
            RawInstruction::PrintByte => write!(f, "."),
            RawInstruction::StoreByte => write!(f, ","),
            RawInstruction::LoopStart => write!(f, "["),
            RawInstruction::LoopEnd => write!(f, "]"),
        }
    }
}

/// Get a description of the effect of the [`RawInstruction`]
impl From<&RawInstruction> for String {
    fn from(raw_instruction: &RawInstruction) -> Self {
        let description = match raw_instruction {
            RawInstruction::IncrementPointer => "Increment the data pointer",
            RawInstruction::DecrementPointer => "Decrement the data pointer",
            RawInstruction::IncrementByte => "Increment the byte at the data pointer.",
            RawInstruction::DecrementByte => "Decrement the byte at the data pointer.",
            RawInstruction::PrintByte => "Output the byte at the data pointer.",
            RawInstruction::StoreByte => "Store to byte at the data pointer.",
            RawInstruction::LoopStart => "Start looping.",
            RawInstruction::LoopEnd => "End looping.",
        };
        description.to_string()
    }
}

#[derive(Error, Debug)]
pub enum BFError {
    /// The brainfuck program contains an unmatched square bracket
    #[error("unmatched {bracket} on line {line} column {column}")]
    UnmatchedBracket {
        bracket: char,
        line: usize,
        column: usize,
    },
    /// Failed to open file containing brainfuck program
    #[error(transparent)]
    IOError(#[from] std::io::Error),
}

#[cfg(test)]
mod tests {
    use crate::RawInstruction;

    #[test]
    fn character_parsing() {
        let chars = vec!['>', '<', '+', '-', '.', ',', '[', ']'];
        let parsed_instructions: Vec<RawInstruction> =
            chars.iter().filter_map(RawInstruction::from_char).collect();
        let expected_instruction = vec![
            RawInstruction::IncrementPointer,
            RawInstruction::DecrementPointer,
            RawInstruction::IncrementByte,
            RawInstruction::DecrementByte,
            RawInstruction::PrintByte,
            RawInstruction::StoreByte,
            RawInstruction::LoopStart,
            RawInstruction::LoopEnd,
        ];
        assert_eq!(parsed_instructions, expected_instruction);
    }
}
